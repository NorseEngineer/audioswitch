EESchema Schematic File Version 2
LIBS:AudioSwitch
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AudioSwitch-cache
EELAYER 25 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 2
Title "Audio Switch"
Date "2017-05-18"
Rev "0.0.1"
Comp "Norse Engineering"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74HC4051PW U?
U 1 1 591E7614
P 8200 1850
F 0 "U?" V 8200 1400 60  0000 C CNN
F 1 "74HC4051PW" H 8200 2450 60  0000 C CNN
F 2 "" H 8100 2200 60  0001 C CNN
F 3 "" H 8100 2200 60  0001 C CNN
	1    8200 1850
	1    0    0    -1  
$EndComp
$Comp
L 74HC4051PW U?
U 1 1 591E764B
P 8200 3450
F 0 "U?" V 8200 3000 60  0000 C CNN
F 1 "74HC4051PW" H 8200 4050 60  0000 C CNN
F 2 "" H 8100 3800 60  0001 C CNN
F 3 "" H 8100 3800 60  0001 C CNN
	1    8200 3450
	1    0    0    -1  
$EndComp
$Comp
L 74HC4051PW U?
U 1 1 591E76D9
P 8200 5050
F 0 "U?" V 8200 4600 60  0000 C CNN
F 1 "74HC4051PW" H 8200 5650 60  0000 C CNN
F 2 "" H 8100 5400 60  0001 C CNN
F 3 "" H 8100 5400 60  0001 C CNN
	1    8200 5050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E796C
P 7250 5850
F 0 "#PWR?" H 7250 5600 50  0001 C CNN
F 1 "GND" H 7250 5700 50  0000 C CNN
F 2 "" H 7250 5850 50  0000 C CNN
F 3 "" H 7250 5850 50  0000 C CNN
	1    7250 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E7986
P 7250 4250
F 0 "#PWR?" H 7250 4000 50  0001 C CNN
F 1 "GND" H 7250 4100 50  0000 C CNN
F 2 "" H 7250 4250 50  0000 C CNN
F 3 "" H 7250 4250 50  0000 C CNN
	1    7250 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E79A0
P 7250 2650
F 0 "#PWR?" H 7250 2400 50  0001 C CNN
F 1 "GND" H 7250 2500 50  0000 C CNN
F 2 "" H 7250 2650 50  0000 C CNN
F 3 "" H 7250 2650 50  0000 C CNN
	1    7250 2650
	1    0    0    -1  
$EndComp
Text Notes 8600 2700 1    60   ~ 0
Left_Audio
Text Notes 8600 4300 1    60   ~ 0
Right_Audio
Text Notes 8600 5900 1    60   ~ 0
Mic
Text Notes 8000 950  0    100  ~ 20
Output
$Comp
L 74HC4051PW U?
U 1 1 591E7C5E
P 2800 1850
F 0 "U?" V 2800 1400 60  0000 C CNN
F 1 "74HC4051PW" H 2800 2450 60  0000 C CNN
F 2 "" H 2700 2200 60  0001 C CNN
F 3 "" H 2700 2200 60  0001 C CNN
	1    2800 1850
	-1   0    0    -1  
$EndComp
$Comp
L 74HC4051PW U?
U 1 1 591E7C64
P 2800 3450
F 0 "U?" V 2800 3000 60  0000 C CNN
F 1 "74HC4051PW" H 2800 4050 60  0000 C CNN
F 2 "" H 2700 3800 60  0001 C CNN
F 3 "" H 2700 3800 60  0001 C CNN
	1    2800 3450
	-1   0    0    -1  
$EndComp
$Comp
L 74HC4051PW U?
U 1 1 591E7C6A
P 2800 5050
F 0 "U?" V 2800 4600 60  0000 C CNN
F 1 "74HC4051PW" H 2800 5650 60  0000 C CNN
F 2 "" H 2700 5400 60  0001 C CNN
F 3 "" H 2700 5400 60  0001 C CNN
	1    2800 5050
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E7C82
P 3750 5850
F 0 "#PWR?" H 3750 5600 50  0001 C CNN
F 1 "GND" H 3750 5700 50  0000 C CNN
F 2 "" H 3750 5850 50  0000 C CNN
F 3 "" H 3750 5850 50  0000 C CNN
	1    3750 5850
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E7C88
P 3750 4250
F 0 "#PWR?" H 3750 4000 50  0001 C CNN
F 1 "GND" H 3750 4100 50  0000 C CNN
F 2 "" H 3750 4250 50  0000 C CNN
F 3 "" H 3750 4250 50  0000 C CNN
	1    3750 4250
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 591E7C8E
P 3750 2650
F 0 "#PWR?" H 3750 2400 50  0001 C CNN
F 1 "GND" H 3750 2500 50  0000 C CNN
F 2 "" H 3750 2650 50  0000 C CNN
F 3 "" H 3750 2650 50  0000 C CNN
	1    3750 2650
	-1   0    0    -1  
$EndComp
Text Notes 2450 2700 1    60   ~ 0
Left_Audio
Text Notes 2450 4300 1    60   ~ 0
Right_Audio
Text Notes 2450 5900 1    60   ~ 0
Mic
Text Notes 3000 950  2    100  ~ 20
Input
Text GLabel 7900 1100 2    61   Input ~ 0
Output_Enable
Text Notes 1050 7450 0    61   Italic 0
TODO\n1- Add amplification stage\n    a- Decide if it should be bypassable\n    b- Add volume buttons\n2- Add uC\n5- Add test points\n6- Add micro usb for power\n7- Add shift registers and leds\n8- Add user buttons\n9- Add programming header\n10- Add UART chip for USB communications
$Comp
L TRRS_35MM J1
U 1 1 591F1B3D
P 1100 1700
F 0 "J1" H 1550 1500 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 1900 60  0000 C CNN
F 2 "" H 1100 1700 60  0001 C CNN
F 3 "" H 1100 1700 60  0001 C CNN
	1    1100 1700
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J2
U 1 1 591F1BC1
P 1100 2250
F 0 "J2" H 1550 2050 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 2450 60  0000 C CNN
F 2 "" H 1100 2250 60  0001 C CNN
F 3 "" H 1100 2250 60  0001 C CNN
	1    1100 2250
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J3
U 1 1 591F1C01
P 1100 2800
F 0 "J3" H 1550 2600 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 3000 60  0000 C CNN
F 2 "" H 1100 2800 60  0001 C CNN
F 3 "" H 1100 2800 60  0001 C CNN
	1    1100 2800
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J4
U 1 1 591F1C6B
P 1100 3350
F 0 "J4" H 1550 3150 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 3550 60  0000 C CNN
F 2 "" H 1100 3350 60  0001 C CNN
F 3 "" H 1100 3350 60  0001 C CNN
	1    1100 3350
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J5
U 1 1 591F1CBF
P 1100 3900
F 0 "J5" H 1550 3700 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 4100 60  0000 C CNN
F 2 "" H 1100 3900 60  0001 C CNN
F 3 "" H 1100 3900 60  0001 C CNN
	1    1100 3900
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J6
U 1 1 591F1D10
P 1100 4450
F 0 "J6" H 1550 4250 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 4650 60  0000 C CNN
F 2 "" H 1100 4450 60  0001 C CNN
F 3 "" H 1100 4450 60  0001 C CNN
	1    1100 4450
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J7
U 1 1 591F1D62
P 1100 5000
F 0 "J7" H 1550 4800 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 5200 60  0000 C CNN
F 2 "" H 1100 5000 60  0001 C CNN
F 3 "" H 1100 5000 60  0001 C CNN
	1    1100 5000
	-1   0    0    1   
$EndComp
$Comp
L TRRS_35MM J8
U 1 1 591F1DBB
P 1100 5550
F 0 "J8" H 1550 5350 60  0000 C CNN
F 1 "TRRS_35MM" H 1300 5750 60  0000 C CNN
F 2 "" H 1100 5550 60  0001 C CNN
F 3 "" H 1100 5550 60  0001 C CNN
	1    1100 5550
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 591F22DF
P 1400 5850
F 0 "#PWR?" H 1400 5600 50  0001 C CNN
F 1 "GND" H 1400 5700 50  0000 C CNN
F 2 "" H 1400 5850 50  0000 C CNN
F 3 "" H 1400 5850 50  0000 C CNN
	1    1400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2450 7450 2450
Wire Wire Line
	7450 800  7450 5850
Wire Wire Line
	7450 4050 7750 4050
Wire Wire Line
	7450 5650 7750 5650
Connection ~ 7450 4050
Connection ~ 7450 2450
Wire Wire Line
	7750 2650 7450 2650
Connection ~ 7450 2650
Wire Wire Line
	7750 4250 7450 4250
Connection ~ 7450 4250
Wire Wire Line
	7450 5850 7750 5850
Connection ~ 7450 5650
Wire Wire Line
	7750 2550 7250 2550
Wire Wire Line
	7250 2550 7250 2650
Wire Wire Line
	7750 4150 7250 4150
Wire Wire Line
	7250 4150 7250 4250
Wire Wire Line
	7750 5750 7250 5750
Wire Wire Line
	7250 5750 7250 5850
Wire Wire Line
	3250 2450 3550 2450
Wire Wire Line
	3550 800  3550 5850
Wire Wire Line
	3550 4050 3250 4050
Wire Wire Line
	3550 5650 3250 5650
Connection ~ 3550 4050
Connection ~ 3550 2450
Wire Wire Line
	3250 2650 3550 2650
Connection ~ 3550 2650
Wire Wire Line
	3250 4250 3550 4250
Connection ~ 3550 4250
Wire Wire Line
	3550 5850 3250 5850
Connection ~ 3550 5650
Wire Wire Line
	3250 2550 3750 2550
Wire Wire Line
	3750 2050 3750 2650
Wire Wire Line
	3250 4150 3750 4150
Wire Wire Line
	3750 3650 3750 4250
Wire Wire Line
	3250 5750 3750 5750
Wire Wire Line
	3750 5250 3750 5850
Wire Wire Line
	3750 2050 3250 2050
Connection ~ 3750 2550
Wire Wire Line
	3750 3650 3250 3650
Connection ~ 3750 4150
Wire Wire Line
	3750 5250 3250 5250
Connection ~ 3750 5750
Wire Wire Line
	4000 3250 3250 3250
Wire Wire Line
	4000 800  4000 4850
Wire Wire Line
	4000 1650 3250 1650
Wire Wire Line
	4000 4850 3250 4850
Connection ~ 4000 3250
Wire Wire Line
	4150 4750 3250 4750
Wire Wire Line
	4150 900  4150 4750
Wire Wire Line
	4150 3150 3250 3150
Wire Wire Line
	4150 1550 3250 1550
Connection ~ 4150 3150
Wire Wire Line
	3250 1450 4300 1450
Wire Wire Line
	4300 1000 4300 4650
Wire Wire Line
	4300 4650 3250 4650
Wire Wire Line
	3250 3050 4300 3050
Connection ~ 4300 3050
Connection ~ 4000 1650
Connection ~ 4150 1550
Connection ~ 4300 1450
Wire Wire Line
	7000 3250 7750 3250
Wire Wire Line
	7000 800  7000 4850
Wire Wire Line
	7000 1650 7750 1650
Wire Wire Line
	7000 4850 7750 4850
Connection ~ 7000 3250
Wire Wire Line
	6850 4750 7750 4750
Wire Wire Line
	6850 900  6850 4750
Wire Wire Line
	6850 3150 7750 3150
Wire Wire Line
	6850 1550 7750 1550
Connection ~ 6850 3150
Wire Wire Line
	7750 1450 6700 1450
Wire Wire Line
	6700 1000 6700 4650
Wire Wire Line
	6700 4650 7750 4650
Wire Wire Line
	7750 3050 6700 3050
Connection ~ 6700 3050
Connection ~ 7000 1650
Connection ~ 6850 1550
Connection ~ 6700 1450
Wire Wire Line
	7650 5250 7750 5250
Wire Wire Line
	7650 3650 7750 3650
Wire Wire Line
	7650 2050 7750 2050
Connection ~ 7650 3650
Connection ~ 7650 2050
Wire Wire Line
	7650 1100 7900 1100
Wire Wire Line
	7650 1100 7650 5250
Wire Wire Line
	1300 1500 1400 1500
Wire Wire Line
	1400 1500 1400 5850
Wire Wire Line
	1400 2050 1300 2050
Wire Wire Line
	1400 2600 1300 2600
Connection ~ 1400 2050
Wire Wire Line
	1400 3150 1300 3150
Connection ~ 1400 2600
Wire Wire Line
	1400 3700 1300 3700
Connection ~ 1400 3150
Wire Wire Line
	1400 4250 1300 4250
Connection ~ 1400 3700
Wire Wire Line
	1400 4800 1300 4800
Connection ~ 1400 4250
Wire Wire Line
	1400 5350 1300 5350
Connection ~ 1400 4800
Connection ~ 1400 5350
Text GLabel 1450 1600 2    60   Input ~ 0
J1L
Text GLabel 1450 1700 2    60   Input ~ 0
J1R
Text GLabel 1450 1800 2    60   Input ~ 0
J1M
Wire Wire Line
	1300 1600 1450 1600
Wire Wire Line
	1300 1700 1450 1700
Wire Wire Line
	1300 1800 1450 1800
Text GLabel 1450 2150 2    60   Input ~ 0
J2L
Text GLabel 1450 2250 2    60   Input ~ 0
J2R
Text GLabel 1450 2350 2    60   Input ~ 0
J2M
Wire Wire Line
	1300 2150 1450 2150
Wire Wire Line
	1300 2250 1450 2250
Wire Wire Line
	1300 2350 1450 2350
Text GLabel 1450 2700 2    60   Input ~ 0
J3L
Text GLabel 1450 2800 2    60   Input ~ 0
J3R
Text GLabel 1450 2900 2    60   Input ~ 0
J3M
Wire Wire Line
	1300 2700 1450 2700
Wire Wire Line
	1300 2800 1450 2800
Wire Wire Line
	1300 2900 1450 2900
Text GLabel 1450 3250 2    60   Input ~ 0
J4L
Text GLabel 1450 3350 2    60   Input ~ 0
J4R
Text GLabel 1450 3450 2    60   Input ~ 0
J4M
Wire Wire Line
	1300 3250 1450 3250
Wire Wire Line
	1300 3350 1450 3350
Wire Wire Line
	1300 3450 1450 3450
Text GLabel 1450 3800 2    60   Input ~ 0
J5L
Text GLabel 1450 3900 2    60   Input ~ 0
J5R
Text GLabel 1450 4000 2    60   Input ~ 0
J5M
Wire Wire Line
	1300 3800 1450 3800
Wire Wire Line
	1300 3900 1450 3900
Wire Wire Line
	1300 4000 1450 4000
Text GLabel 1450 4350 2    60   Input ~ 0
J6L
Text GLabel 1450 4450 2    60   Input ~ 0
J6R
Text GLabel 1450 4550 2    60   Input ~ 0
J6M
Wire Wire Line
	1300 4350 1450 4350
Wire Wire Line
	1300 4450 1450 4450
Wire Wire Line
	1300 4550 1450 4550
Text GLabel 1450 4900 2    60   Input ~ 0
J7L
Text GLabel 1450 5000 2    60   Input ~ 0
J7R
Text GLabel 1450 5100 2    60   Input ~ 0
J7M
Wire Wire Line
	1300 4900 1450 4900
Wire Wire Line
	1300 5000 1450 5000
Wire Wire Line
	1300 5100 1450 5100
Text GLabel 1450 5450 2    60   Input ~ 0
J8L
Text GLabel 1450 5550 2    60   Input ~ 0
J8R
Text GLabel 1450 5650 2    60   Input ~ 0
J8M
Wire Wire Line
	1300 5450 1450 5450
Wire Wire Line
	1300 5550 1450 5550
Wire Wire Line
	1300 5650 1450 5650
Text GLabel 2150 1450 0    60   Input ~ 0
J1L
Text GLabel 2150 1550 0    60   Input ~ 0
J2L
Text GLabel 2150 1650 0    60   Input ~ 0
J3L
Text GLabel 2150 1750 0    60   Input ~ 0
J4L
Text GLabel 2150 1850 0    60   Input ~ 0
J5L
Text GLabel 2150 1950 0    60   Input ~ 0
J6L
Text GLabel 2150 2050 0    60   Input ~ 0
J7L
Text GLabel 2150 2150 0    60   Input ~ 0
J8L
Wire Wire Line
	2150 1450 2300 1450
Wire Wire Line
	2150 1550 2300 1550
Wire Wire Line
	2150 1650 2300 1650
Wire Wire Line
	2150 1750 2300 1750
Wire Wire Line
	2150 1850 2300 1850
Wire Wire Line
	2150 1950 2300 1950
Wire Wire Line
	2150 2050 2300 2050
Wire Wire Line
	2150 2150 2300 2150
Text GLabel 2200 3050 0    60   Input ~ 0
J1R
Text GLabel 2200 3150 0    60   Input ~ 0
J2R
Text GLabel 2200 3250 0    60   Input ~ 0
J3R
Text GLabel 2200 3350 0    60   Input ~ 0
J4R
Text GLabel 2200 3450 0    60   Input ~ 0
J5R
Text GLabel 2200 3550 0    60   Input ~ 0
J6R
Text GLabel 2200 3650 0    60   Input ~ 0
J7R
Text GLabel 2200 3750 0    60   Input ~ 0
J8R
Wire Wire Line
	2200 3050 2300 3050
Wire Wire Line
	2200 3150 2300 3150
Wire Wire Line
	2200 3250 2300 3250
Wire Wire Line
	2200 3350 2300 3350
Wire Wire Line
	2200 3450 2300 3450
Wire Wire Line
	2200 3550 2300 3550
Wire Wire Line
	2200 3650 2300 3650
Wire Wire Line
	2200 3750 2300 3750
Text GLabel 2200 5350 0    60   Input ~ 0
J8M
Text GLabel 2200 5250 0    60   Input ~ 0
J7M
Text GLabel 2200 5150 0    60   Input ~ 0
J6M
Text GLabel 2200 5050 0    60   Input ~ 0
J5M
Text GLabel 2200 4950 0    60   Input ~ 0
J4M
Text GLabel 2200 4850 0    60   Input ~ 0
J3M
Text GLabel 2200 4750 0    60   Input ~ 0
J2M
Text GLabel 2200 4650 0    60   Input ~ 0
J1M
Wire Wire Line
	2200 4650 2300 4650
Wire Wire Line
	2200 4750 2300 4750
Wire Wire Line
	2200 4850 2300 4850
Wire Wire Line
	2200 4950 2300 4950
Wire Wire Line
	2200 5050 2300 5050
Wire Wire Line
	2200 5150 2300 5150
Wire Wire Line
	2300 5250 2200 5250
Wire Wire Line
	2200 5350 2300 5350
$Comp
L TRRS_35MM J9
U 1 1 591F59FD
P 9900 1700
F 0 "J9" H 10350 1500 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 1900 60  0000 C CNN
F 2 "" H 9900 1700 60  0001 C CNN
F 3 "" H 9900 1700 60  0001 C CNN
	1    9900 1700
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J10
U 1 1 591F5A03
P 9900 2250
F 0 "J10" H 10400 2050 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 2450 60  0000 C CNN
F 2 "" H 9900 2250 60  0001 C CNN
F 3 "" H 9900 2250 60  0001 C CNN
	1    9900 2250
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J11
U 1 1 591F5A09
P 9900 2800
F 0 "J11" H 10400 2600 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 3000 60  0000 C CNN
F 2 "" H 9900 2800 60  0001 C CNN
F 3 "" H 9900 2800 60  0001 C CNN
	1    9900 2800
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J12
U 1 1 591F5A0F
P 9900 3350
F 0 "J12" H 10400 3150 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 3550 60  0000 C CNN
F 2 "" H 9900 3350 60  0001 C CNN
F 3 "" H 9900 3350 60  0001 C CNN
	1    9900 3350
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J13
U 1 1 591F5A15
P 9900 3900
F 0 "J13" H 10400 3700 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 4100 60  0000 C CNN
F 2 "" H 9900 3900 60  0001 C CNN
F 3 "" H 9900 3900 60  0001 C CNN
	1    9900 3900
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J14
U 1 1 591F5A1B
P 9900 4450
F 0 "J14" H 10400 4250 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 4650 60  0000 C CNN
F 2 "" H 9900 4450 60  0001 C CNN
F 3 "" H 9900 4450 60  0001 C CNN
	1    9900 4450
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J15
U 1 1 591F5A21
P 9900 5000
F 0 "J15" H 10400 4800 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 5200 60  0000 C CNN
F 2 "" H 9900 5000 60  0001 C CNN
F 3 "" H 9900 5000 60  0001 C CNN
	1    9900 5000
	1    0    0    1   
$EndComp
$Comp
L TRRS_35MM J16
U 1 1 591F5A27
P 9900 5550
F 0 "J16" H 10400 5350 60  0000 C CNN
F 1 "TRRS_35MM" H 10100 5750 60  0000 C CNN
F 2 "" H 9900 5550 60  0001 C CNN
F 3 "" H 9900 5550 60  0001 C CNN
	1    9900 5550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 591F5A2D
P 9600 5850
F 0 "#PWR?" H 9600 5600 50  0001 C CNN
F 1 "GND" H 9600 5700 50  0000 C CNN
F 2 "" H 9600 5850 50  0000 C CNN
F 3 "" H 9600 5850 50  0000 C CNN
	1    9600 5850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9700 1500 9600 1500
Wire Wire Line
	9600 1500 9600 5850
Wire Wire Line
	9600 2050 9700 2050
Wire Wire Line
	9600 2600 9700 2600
Connection ~ 9600 2050
Wire Wire Line
	9600 3150 9700 3150
Connection ~ 9600 2600
Wire Wire Line
	9600 3700 9700 3700
Connection ~ 9600 3150
Wire Wire Line
	9600 4250 9700 4250
Connection ~ 9600 3700
Wire Wire Line
	9600 4800 9700 4800
Connection ~ 9600 4250
Wire Wire Line
	9600 5350 9700 5350
Connection ~ 9600 4800
Connection ~ 9600 5350
Text GLabel 9550 1600 0    60   Input ~ 0
J9L
Text GLabel 9550 1700 0    60   Input ~ 0
J9R
Text GLabel 9550 1800 0    60   Input ~ 0
J9M
Wire Wire Line
	9700 1600 9550 1600
Wire Wire Line
	9700 1700 9550 1700
Wire Wire Line
	9700 1800 9550 1800
Text GLabel 9550 2150 0    60   Input ~ 0
J10L
Text GLabel 9550 2250 0    60   Input ~ 0
J10R
Text GLabel 9550 2350 0    60   Input ~ 0
J10M
Wire Wire Line
	9700 2150 9550 2150
Wire Wire Line
	9700 2250 9550 2250
Wire Wire Line
	9700 2350 9550 2350
Text GLabel 9550 2700 0    60   Input ~ 0
J11L
Text GLabel 9550 2800 0    60   Input ~ 0
J11R
Text GLabel 9550 2900 0    60   Input ~ 0
J11M
Wire Wire Line
	9700 2700 9550 2700
Wire Wire Line
	9700 2800 9550 2800
Wire Wire Line
	9700 2900 9550 2900
Text GLabel 9550 3250 0    60   Input ~ 0
J12L
Text GLabel 9550 3350 0    60   Input ~ 0
J12R
Text GLabel 9550 3450 0    60   Input ~ 0
J12M
Wire Wire Line
	9700 3250 9550 3250
Wire Wire Line
	9700 3350 9550 3350
Wire Wire Line
	9700 3450 9550 3450
Text GLabel 9550 3800 0    60   Input ~ 0
J13L
Text GLabel 9550 3900 0    60   Input ~ 0
J13R
Text GLabel 9550 4000 0    60   Input ~ 0
J13M
Wire Wire Line
	9700 3800 9550 3800
Wire Wire Line
	9700 3900 9550 3900
Wire Wire Line
	9700 4000 9550 4000
Text GLabel 9550 4350 0    60   Input ~ 0
J14L
Text GLabel 9550 4450 0    60   Input ~ 0
J14R
Text GLabel 9550 4550 0    60   Input ~ 0
J14M
Wire Wire Line
	9700 4350 9550 4350
Wire Wire Line
	9700 4450 9550 4450
Wire Wire Line
	9700 4550 9550 4550
Text GLabel 9550 4900 0    60   Input ~ 0
J15L
Text GLabel 9550 5000 0    60   Input ~ 0
J15R
Text GLabel 9550 5100 0    60   Input ~ 0
J15M
Wire Wire Line
	9700 4900 9550 4900
Wire Wire Line
	9700 5000 9550 5000
Wire Wire Line
	9700 5100 9550 5100
Text GLabel 9550 5450 0    60   Input ~ 0
J16L
Text GLabel 9550 5550 0    60   Input ~ 0
J16R
Text GLabel 9550 5650 0    60   Input ~ 0
J16M
Wire Wire Line
	9700 5450 9550 5450
Wire Wire Line
	9700 5550 9550 5550
Wire Wire Line
	9700 5650 9550 5650
Text GLabel 8850 1450 2    60   Input ~ 0
J9L
Text GLabel 8850 1550 2    60   Input ~ 0
J10L
Text GLabel 8850 1650 2    60   Input ~ 0
J11L
Text GLabel 8850 1750 2    60   Input ~ 0
J12L
Text GLabel 8850 1850 2    60   Input ~ 0
J13L
Text GLabel 8850 1950 2    60   Input ~ 0
J14L
Text GLabel 8850 2050 2    60   Input ~ 0
J15L
Text GLabel 8850 2150 2    60   Input ~ 0
J16L
Wire Wire Line
	8850 1450 8700 1450
Wire Wire Line
	8850 1550 8700 1550
Wire Wire Line
	8850 1650 8700 1650
Wire Wire Line
	8850 1750 8700 1750
Wire Wire Line
	8850 1850 8700 1850
Wire Wire Line
	8850 1950 8700 1950
Wire Wire Line
	8850 2050 8700 2050
Wire Wire Line
	8850 2150 8700 2150
Text GLabel 8800 3050 2    60   Input ~ 0
J9R
Text GLabel 8800 3150 2    60   Input ~ 0
J10R
Text GLabel 8800 3250 2    60   Input ~ 0
J11R
Text GLabel 8800 3350 2    60   Input ~ 0
J12R
Text GLabel 8800 3450 2    60   Input ~ 0
J13R
Text GLabel 8800 3550 2    60   Input ~ 0
J14R
Text GLabel 8800 3650 2    60   Input ~ 0
J15R
Text GLabel 8800 3750 2    60   Input ~ 0
J16R
Wire Wire Line
	8800 3050 8700 3050
Wire Wire Line
	8800 3150 8700 3150
Wire Wire Line
	8800 3250 8700 3250
Wire Wire Line
	8800 3350 8700 3350
Wire Wire Line
	8800 3450 8700 3450
Wire Wire Line
	8800 3550 8700 3550
Wire Wire Line
	8800 3650 8700 3650
Wire Wire Line
	8800 3750 8700 3750
Text GLabel 8800 5350 2    60   Input ~ 0
J16M
Text GLabel 8800 5250 2    60   Input ~ 0
J15M
Text GLabel 8800 5150 2    60   Input ~ 0
J14M
Text GLabel 8800 5050 2    60   Input ~ 0
J13M
Text GLabel 8800 4950 2    60   Input ~ 0
J12M
Text GLabel 8800 4850 2    60   Input ~ 0
J11M
Text GLabel 8800 4750 2    60   Input ~ 0
J10M
Text GLabel 8800 4650 2    60   Input ~ 0
J9M
Wire Wire Line
	8800 4650 8700 4650
Wire Wire Line
	8800 4750 8700 4750
Wire Wire Line
	8800 4850 8700 4850
Wire Wire Line
	8800 4950 8700 4950
Wire Wire Line
	8800 5050 8700 5050
Wire Wire Line
	8800 5150 8700 5150
Wire Wire Line
	8700 5250 8800 5250
Wire Wire Line
	8800 5350 8700 5350
$Comp
L +5V #PWR?
U 1 1 591F74CC
P 3550 800
F 0 "#PWR?" H 3550 650 50  0001 C CNN
F 1 "+5V" H 3550 940 50  0000 C CNN
F 2 "" H 3550 800 50  0000 C CNN
F 3 "" H 3550 800 50  0000 C CNN
	1    3550 800 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 591F766C
P 7450 800
F 0 "#PWR?" H 7450 650 50  0001 C CNN
F 1 "+5V" H 7450 940 50  0000 C CNN
F 2 "" H 7450 800 50  0000 C CNN
F 3 "" H 7450 800 50  0000 C CNN
	1    7450 800 
	1    0    0    -1  
$EndComp
Text GLabel 4300 1000 2    60   Input ~ 0
INPUT_S2
Text GLabel 4300 900  2    60   Input ~ 0
INPUT_S1
Text GLabel 4300 800  2    60   Input ~ 0
INPUT_S0
Wire Wire Line
	4150 900  4300 900 
Wire Wire Line
	4000 800  4300 800 
Text GLabel 6700 800  0    60   Input ~ 0
OUTPUT_S2
Text GLabel 6700 900  0    60   Input ~ 0
OUTPUT_S1
Text GLabel 6700 1000 0    60   Input ~ 0
OUTPUT_S0
Wire Wire Line
	6850 900  6700 900 
Wire Wire Line
	7000 800  6700 800 
Wire Wire Line
	3250 2150 4500 2150
Wire Wire Line
	3250 3750 4500 3750
Wire Wire Line
	3250 5350 4650 5350
Wire Wire Line
	6400 5350 7750 5350
Wire Wire Line
	6500 3750 7750 3750
Wire Wire Line
	6400 2150 7750 2150
$Sheet
S 4950 2650 1000 800 
U 591FA5D2
F0 "AmpSheet" 60
F1 "Amplification.sch" 60
F2 "MIC_IN" I L 4950 2950 60 
F3 "L_AUDIO_IN" I L 4950 2750 60 
F4 "R_AUDIO_IN" I L 4950 2850 60 
F5 "MIC_OUT" O R 5950 3350 60 
F6 "L_AUDIO_OUT" O R 5950 3150 60 
F7 "R_AUDIO_OUT" O R 5950 3250 60 
$EndSheet
Wire Wire Line
	4950 2750 4500 2750
Wire Wire Line
	4500 2750 4500 2150
Wire Wire Line
	4500 3750 4500 2850
Wire Wire Line
	4500 2850 4950 2850
Wire Wire Line
	4650 5350 4650 2950
Wire Wire Line
	4650 2950 4950 2950
Wire Wire Line
	6400 2150 6400 3150
Wire Wire Line
	6400 3150 5950 3150
Wire Wire Line
	6500 3250 6500 3750
Wire Wire Line
	6500 3250 5950 3250
Wire Wire Line
	6400 5350 6400 3350
Wire Wire Line
	6400 3350 5950 3350
$EndSCHEMATC
