EESchema Schematic File Version 2
LIBS:AudioSwitch
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AudioSwitch-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "Audio Switch"
Date "2017-05-18"
Rev "0.0.1"
Comp "Norse Engineering"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4000 1400 0    60   Input ~ 0
MIC_IN
Text HLabel 4000 1650 0    60   Input ~ 0
L_AUDIO_IN
Text HLabel 4000 1900 0    60   Input ~ 0
R_AUDIO_IN
Text HLabel 6750 1350 2    60   Output ~ 0
MIC_OUT
Text HLabel 6750 1550 2    60   Output ~ 0
L_AUDIO_OUT
Text HLabel 6750 1700 2    60   Output ~ 0
R_AUDIO_OUT
$EndSCHEMATC
